
<?php
function ubah_huruf($string){
//kode di sini
    $abjad = 'abcdefghijklmnopqrstuvwxyz';
    $res = '';

    for ($i=0; $i < strlen($string); $i++) { 
        $posisi = strpos($abjad, $string[$i]);
        $res .= substr($abjad, $posisi+1, 1);
    }

    return $res."<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>